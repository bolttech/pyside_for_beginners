import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


def main():
    app = QApplication(sys.argv)  # создали экземпляр приложения
    win = QTreeView()  # создали окно
    model = QFileSystemModel()
    model.setRootPath("D:")
    win.setModel(model)
    win.setWindowTitle("Файловая система")
    win.show()  # показать окно
    sys.exit(app.exec_())  # запуск приложения


if __name__ == "__main__":
    main()
