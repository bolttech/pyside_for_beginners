import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


def main():
    app = QApplication(sys.argv)  # создали экземпляр приложения
    win = QWidget()  # создали окно
    win.setWindowTitle("Привет, мир!")

    main_layout = QVBoxLayout()
    input_layout = QHBoxLayout()

    name_edit = QLineEdit()
    greet_button = QPushButton("Нажми меня")
    greet_label = QLabel("Это не тот текст, что вы ищете")

    input_layout.addWidget(name_edit)
    input_layout.addWidget(greet_button)

    main_layout.addLayout(input_layout)
    main_layout.addWidget(greet_label)

    win.setLayout(main_layout)

    win.show()  # показать окно
    sys.exit(app.exec_())  # запуск приложения


if __name__ == "__main__":
    main()
