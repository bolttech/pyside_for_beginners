import sys
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from main_window import MainWindow


def main():
    app = QApplication(sys.argv)  # создали экземпляр приложения
    win = MainWindow()  # создали окно
    win.show()  # показать окно
    sys.exit(app.exec_())  # запуск приложения


if __name__ == "__main__":
    main()
