# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'person_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.0.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_EditPersonDialog(object):
    def setupUi(self, EditPersonDialog):
        if not EditPersonDialog.objectName():
            EditPersonDialog.setObjectName(u"EditPersonDialog")
        EditPersonDialog.resize(412, 130)
        self.verticalLayout = QVBoxLayout(EditPersonDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(EditPersonDialog)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.lastNameEdit = QLineEdit(EditPersonDialog)
        self.lastNameEdit.setObjectName(u"lastNameEdit")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.lastNameEdit)

        self.label_2 = QLabel(EditPersonDialog)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.firstNameEdit = QLineEdit(EditPersonDialog)
        self.firstNameEdit.setObjectName(u"firstNameEdit")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.firstNameEdit)

        self.label_3 = QLabel(EditPersonDialog)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_3)

        self.middleNameEdit = QLineEdit(EditPersonDialog)
        self.middleNameEdit.setObjectName(u"middleNameEdit")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.middleNameEdit)


        self.verticalLayout.addLayout(self.formLayout)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer)

        self.okButton = QPushButton(EditPersonDialog)
        self.okButton.setObjectName(u"okButton")

        self.horizontalLayout_4.addWidget(self.okButton)

        self.cancelButton = QPushButton(EditPersonDialog)
        self.cancelButton.setObjectName(u"cancelButton")

        self.horizontalLayout_4.addWidget(self.cancelButton)


        self.verticalLayout.addLayout(self.horizontalLayout_4)


        self.retranslateUi(EditPersonDialog)
        self.cancelButton.clicked.connect(EditPersonDialog.reject)

        QMetaObject.connectSlotsByName(EditPersonDialog)
    # setupUi

    def retranslateUi(self, EditPersonDialog):
        EditPersonDialog.setWindowTitle(QCoreApplication.translate("EditPersonDialog", u"\u0421\u0432\u0435\u0434\u0435\u043d\u0438\u044f \u043e \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u0443\u0435\u043c\u043e\u043c \u0447\u0435\u043b\u043e\u0432\u0435\u043a\u0435", None))
        self.label.setText(QCoreApplication.translate("EditPersonDialog", u"\u0424\u0430\u043c\u0438\u043b\u0438\u044f:", None))
        self.label_2.setText(QCoreApplication.translate("EditPersonDialog", u"\u0418\u043c\u044f:", None))
        self.label_3.setText(QCoreApplication.translate("EditPersonDialog", u"\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e:", None))
        self.okButton.setText(QCoreApplication.translate("EditPersonDialog", u"OK", None))
        self.cancelButton.setText(QCoreApplication.translate("EditPersonDialog", u"\u041e\u0442\u043c\u0435\u043d\u0430", None))
    # retranslateUi

