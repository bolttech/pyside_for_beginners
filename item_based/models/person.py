from dataclasses import dataclass


@dataclass
class Person:
    last_name: str
    first_name: str
    middle_name: str
