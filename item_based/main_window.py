from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from ui.main_window import Ui_MainWindow
from models.person import Person
from person_dialog import PersonDialog


class MainWindow(QWidget):
    def __init__(self):
        super().__init__(parent=None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setup_signals()
        self.people = [
            Person("Иванов", "Иван", "Иванович"),
            Person("Петров", "Петр", "Петрович"),
            Person("Сергеев", "Сергей", "Сергеевич")
        ]
        self.__fill_list()

    def __fill_list(self):
        for person in self.people:
            self.__add_person(person)

    def __add_person(self, person: Person):
        item = QListWidgetItem(f"{person.last_name} {person.first_name} {person.middle_name}", self.ui.personList)

    def setup_signals(self):
        self.ui.addPersonButton.clicked.connect(self.__handle_add)
        self.ui.removePersonButton.clicked.connect(self.__handle_remove)
        self.ui.personList.itemDoubleClicked.connect(self.__handle_edit)

    def __handle_add(self):
        person = PersonDialog.add_person()
        self.__add_person(person)

    def __handle_remove(self):
        row = self.ui.personList.currentRow()
        if row >= 0:
            person = self.people[row]
            result = QMessageBox.question(
                self,
                "Удаление",
                "Вы уверены, что хотите удалить человека по имени " +
                f"{person.last_name} {person.first_name} {person.middle_name}?",
                QMessageBox.Yes,
                QMessageBox.No
            )
            if result == QMessageBox.Yes:
                del self.people[row]
                self.ui.personList.takeItem(row)

    def __handle_edit(self, item: QListWidgetItem):
        row = self.ui.personList.row(item)
        person = self.people[row]
        PersonDialog.edit_person(person)
        item.setText(f"{person.last_name} {person.first_name} {person.middle_name}")
