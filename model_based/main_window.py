from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from ui.main_window import Ui_MainWindow
from models.person import Person
from models.person_list_model import PersonListModel
from person_dialog import PersonDialog


class MainWindow(QWidget):
    def __init__(self):
        super().__init__(parent=None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setup_signals()
        self.model = PersonListModel([
            Person("Иванов", "Иван", "Иванович"),
            Person("Петров", "Петр", "Петрович"),
            Person("Сергеев", "Сергей", "Сергеевич")
        ])
        self.ui.personView.setModel(self.model)

    def setup_signals(self):
        self.ui.addPersonButton.clicked.connect(self.__handle_add)
        self.ui.removePersonButton.clicked.connect(self.__handle_remove)
        self.ui.personView.doubleClicked.connect(self.__handle_edit)

    def __handle_add(self):
        person = PersonDialog.add_person()
        self.model.add_person(person)

    def __handle_remove(self):
        row = self.ui.personView.currentIndex().row()
        if row >= 0:
            person = self.model.get_person(row)
            result = QMessageBox.question(
                self,
                "Удаление",
                "Вы уверены, что хотите удалить человека по имени " +
                f"{person.last_name} {person.first_name} {person.middle_name}?",
                QMessageBox.Yes,
                QMessageBox.No
            )
            if result == QMessageBox.Yes:
                self.model.remove_person(row)

    def __handle_edit(self, index: QModelIndex):
        row = index.row()
        person = self.model.get_person(row)
        PersonDialog.edit_person(person)
        self.model.update_person(row)
