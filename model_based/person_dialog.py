from typing import Optional
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from ui.person_dialog import Ui_EditPersonDialog
from models.person import Person


class PersonDialog(QDialog):
    def __init__(self):
        super().__init__(parent=None)
        self.ui = Ui_EditPersonDialog()
        self.ui.setupUi(self)
        self.ui.okButton.clicked.connect(self.__handle_ok)

    @staticmethod
    def add_person() -> Optional[Person]:
        dialog = PersonDialog()
        result = dialog.exec_()
        if result == QDialog.Rejected:
            return None
        return Person(
            dialog.ui.lastNameEdit.text(),
            dialog.ui.firstNameEdit.text(),
            dialog.ui.middleNameEdit.text()
        )

    @staticmethod
    def edit_person(person: Person):
        dialog = PersonDialog()

        dialog.ui.lastNameEdit.setText(person.last_name)
        dialog.ui.firstNameEdit.setText(person.first_name)
        dialog.ui.middleNameEdit.setText(person.middle_name)

        result = dialog.exec_()
        if result == QDialog.Rejected:
            return

        person.last_name = dialog.ui.lastNameEdit.text()
        person.first_name = dialog.ui.firstNameEdit.text()
        person.middle_name = dialog.ui.middleNameEdit.text()

    def __handle_ok(self):
        if not self.ui.lastNameEdit.text():
            self.ui.lastNameEdit.setFocus()
            return
        if not self.ui.firstNameEdit.text():
            self.ui.firstNameEdit.setFocus()
            return
        if not self.ui.middleNameEdit.text():
            self.ui.middleNameEdit.setFocus()
            return
        self.accept()
