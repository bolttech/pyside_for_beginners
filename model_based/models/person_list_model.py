from typing import Any, Optional
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from .person import Person


class PersonListModel(QAbstractListModel):
    people_list: list[Person]

    def __init__(self, people_list, parent=None):
        super().__init__(parent=parent)
        self.people_list = people_list

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return len(self.people_list)

    def data(self, index: QModelIndex, role: int = Qt.DisplayRole) -> Any:
        if role == Qt.DisplayRole:
            row = index.row()
            person: Person = self.people_list[row]
            return f"{person.last_name} {person.first_name} {person.middle_name}"

    def get_person(self, row: int) -> Person:
        return self.people_list[row]

    def add_person(self, person: Optional[Person]):
        if person is not None:
            self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
            self.people_list.append(person)
            self.endInsertRows()

    def remove_person(self, row: int):
        if 0 <= row < self.rowCount():
            self.beginRemoveRows(QModelIndex(), row, row)
            del self.people_list[row]
            self.endRemoveRows()

    def update_person(self, row: int):
        index = self.index(row)
        self.dataChanged.emit(index, index)
