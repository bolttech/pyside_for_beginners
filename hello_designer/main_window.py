from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from ui.main_window import Ui_MainWindow


class MainWindow(QWidget):
    def __init__(self):
        super().__init__(parent=None)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setup_signals()

    def setup_signals(self):
        self.ui.greet_button.clicked.connect(self.greet)

    def greet(self):
        name = self.ui.name_edit.text()
        if not name:
            name = "незнакомец"
        greeting = f"Привет, {name}"
        self.ui.greet_label.setText(greeting)
